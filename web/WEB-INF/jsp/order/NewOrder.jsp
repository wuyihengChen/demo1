<%@ include file="../common/IncludeTop.jsp"%>
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
<div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#Catalog").hide().fadeIn();
        });
    </script>
</div>
<div id="Catalog">
    <form action = "newOrder" method="post">
        <!--  标题进度条 start-->
        <div class="content" style="margin: 2% 30%;width: 100%;">
            <div class="processBar">
                <div id="line0" class="bar">
                    <div id="point0" class="c-step c-select"></div>
                </div>
                <div class="text" style="margin: 10px -25px;"><span class='poetry1'>Payment Details</span></div>
            </div>
            <div class="processBar">
                <div id="line1" class="bar">
                    <div id="point1" class="c-step"></div>
                </div>
                <div class="text" style="margin: 10px -30px;"><span class='poetry2'>Billing Address</span></div>
            </div>
            <div class="processBar">
                <div id="line2" class="bar" style="width: 0;">
                    <div id="point2" class="c-step"></div>
                </div>
                <div class="text" style="margin: 10px -30px;"><span class='poetry3'>Shipping Address</span></div>
            </div>
        </div>
        <!--  标题进度条 end-->
        <div style="clear: both;"></div>
        <div id="MainContent" style="margin: 2% 30%;">
            <div class="content" id="basicInfo">
                <table>
                    <tr>
                        <th colspan=2>Payment Details</th>
                    </tr>
                    <tr>
                        <td>Card Type:</td>
                        <td><select name="cardType">
                            <option selected="selected" value="Visa">Visa</option>
                            <option value="MasterCard">MasterCard</option>
                            <option value="American Express">American Express</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td>Card Number:</td>
                        <td><input type = "text" name = "creditCard" value="${sessionScope.order.creditCard}"/></td>
                    </tr>
                    <tr>
                        <td>Expiry Date (MM/YYYY):</td>
                        <td><input type = "text" name = "expiryDate" value="${sessionScope.order.expiryDate}"/></td>
                    </tr>
                </table>
            </div>
            <div class="content" id="education">
                <table>
                    <tr>
                        <th colspan=2>Billing Address</th>
                    </tr>
                    <tr>
                        <td>First name:</td>
                        <td><input type = "text" name = "billToFirstName" value="${sessionScope.account.firstName}"/></td>
                    </tr>
                    <tr>
                        <td>Last name:</td>
                        <td><input type = "text" name = "billToLastName" value="${sessionScope.account.lastName}"/></td>

                    </tr>
                    <tr>
                        <td>Address 1:</td>
                        <td><input type = "text" name = "billAddress1" value="${sessionScope.account.address1}"/></td>
                    </tr>
                    <tr>
                        <td>Address 2:</td>
                        <td><input type = "text" name = "billAddress2" value="${sessionScope.account.address2}"/></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><input type = "text" name = "billCity" value="${sessionScope.account.city}"/></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td><input type = "text" name = "billState" value="${sessionScope.account.state}"/></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td><input type = "text" name = "billZip" value="${sessionScope.account.zip}"/></td>

                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td><input type = "text" name = "billCountry" value="${sessionScope.account.country}"/></td>

                    </tr>
                </table>
            </div>
            <div class="content" id="work">
                <table>
                    <tr>
                        <th colspan=2>Shipping Address</th>
                    </tr>

                    <tr>
                        <td>First name:</td>
                        <td><input type = "text" name = "shipToFirstName" value="${sessionScope.account.firstName}"/></td>
                    </tr>
                    <tr>
                        <td>Last name:</td>
                        <td><input type = "text" name = "shipToLastName" value="${sessionScope.account.lastName}"/></td>

                    </tr>
                    <tr>
                        <td>Address 1:</td>
                        <td><input type = "text" name = "shipAddress1" value="${sessionScope.account.address1}"/></td>
                    </tr>
                    <tr>
                        <td>Address 2:</td>
                        <td><input type = "text" name = "shipAddress2" value="${sessionScope.account.address2}"/></td>
                    </tr>
                    <tr>
                        <td>City:</td>
                        <td><input type = "text" name = "shipCity" value="${sessionScope.account.city}"/></td>
                    </tr>
                    <tr>
                        <td>State:</td>
                        <td><input type = "text" name = "shipState" value="${sessionScope.account.state}"/></td>
                    </tr>
                    <tr>
                        <td>Zip:</td>
                        <td><input type = "text" name = "shipZip" value="${sessionScope.account.zip}"/></td>

                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td><input type = "text" name = "shipCountry" value="${sessionScope.account.country}"/></td>
                    </tr>

                </table>
            </div>

        </div>
        <div style="clear: both;"></div>
        <div style="text-align: center;">
            <button type="button" id="previous_step">BACK</button>
            <button type="button" id="next_step">NEXT</button>
            <button id="finish">FINISH</button>
        </div>
        <%--onclick="window.location.href='ComfireServlet'--%>
    </form></div>
<%@include file="../common/IncludeBottom.jsp"%>

<script type="text/javascript" src="js/index.js"> </script>