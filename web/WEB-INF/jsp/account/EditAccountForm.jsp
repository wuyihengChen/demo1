<%@ include file="../common/IncludeTop.jsp"%>
<div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#Catalog").hide().fadeIn();
        });
    </script>
</div>
<div id="Catalog">
    <form action="editAccount?account=${sessionScope.account}" method="post">
    <h3>User Information</h3>

    <table>
        <tr>
            <td>User ID:</td>
            <td>${sessionScope.account.username}</td>
        </tr>
        <tr>
            <td>New password:</td>
            <td><input type="text" name="password" /></td>
        </tr>
        <tr>
            <td>Repeat password:</td>
            <td><input type="text" name="repeatedPassword" /></td>

                <font color="red">${requestScope.msg}</font>

        </tr>
    </table>
        <%@ include file="IncludeAccountFields.jsp"%>

        <input type="submit" name="editAccount" value="Save Account Information" />

</form>
    <a href="listOrders">My Orders</a>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>
