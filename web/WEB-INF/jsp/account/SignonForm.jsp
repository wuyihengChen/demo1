<%@ include file="../common/IncludeTop.jsp"%>
<div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#Catalog").hide().fadeIn();
        });
    </script>
</div>
<div id="Catalog">
    <form action="signon" method="post">
        <p class="pcss">Please enter your username and password.</p>
        <div class="login">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form" >
				<span class="login100-form-title">
					Login
				</span>

                        <div class="wrap-input100 validate-input">
                            <input class="input100" type="text" name="username" placeholder="Account" >
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
						<i class="fa fa-envelope" aria-hidden="true"></i>
                        <span id="usernameTips"></span>
					</span>
                        </div>

                        <div class="wrap-input100 validate-input">
                            <input class="input100" type="password" name="password" placeholder="Password" >
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</span>
                        </div>

                        <div class="vcodediv">
                            <span>VerificationCode:</span>
                            <input type="text" name="code" size="5" maxlength="5" class="vcode"/>
                            <a href="http://localhost:8080/mypetstore/Kaptcha.jpg"><img src="http://localhost:8080/mypetstore/Kaptcha.jpg" style="vertical-align: middle; height: 20px ;"></a>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn">
                                Go
                            </button>
                        </div>
                        <font color="red" class="msg"><br>${requestScope.msg}</br></font>
                    </form>
                </div>
            </div>
        </div>
    </form>
    Need a user name and password?
    <a class="acss" href="newAccountForm">Register Now!</a>
</div>

<%@ include file="../common/IncludeBottom.jsp"%>

<link rel="stylesheet" href="css/signon.css" type="text/css" media="screen"/>