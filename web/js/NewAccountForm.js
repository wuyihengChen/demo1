//1.用户合法性判断
var reN = /^\w{3,20}$/
$('#username').on('blur', function () {
    if (this.value.trim() == '') {
        $('#usernameTips').attr("class", "errorTips").text("用户名不为空");
        return;
    }
    if (reN.test(this.value)) {
        $.ajax({
            type: "GET",
            url: "UsernameExist?username=" + this.value,
            success: function (data) {
                if (data.code == 1)
                    $('#usernameTips').attr("class", "errorTips").text("用户名已存在，请重新输入");
                else if (data.code == 0)
                    $('#usernameTips').attr("class", "okTips").text("Available");
            }
        })
    }
    else
        $('#usernameTips').attr("class","errorTips").text("用户名是3-20位数字、字母和下划线！");
})

//2.密码合法性判断
var reP = /^[\w!-@#$%^&*]{6,20}$/
$('#password').on('blur',function () {
    if(this.value.trim() == '')
        $('#passwordTips').attr("class","errorTips").text("密码不为空");
    else if(!reP.test(this.value))
        $('#passwordTips').attr("class","errorTips").text("密码是6到20位字母、数字，还可包含@!#$%^&*-字符");
    else
        $('#passwordTips').attr("class","okTips").text("Available");
})

//3.判断两次输入密码一致性
$('#repeatedPassword').on('blur',function (){
    if(this.value.trim() == '')
        $('#repeatedPasswordTips').attr("class","errorTips").text("重复密码不为空");
    else if(!($('#password').val() == this.value))
        $('#repeatedPasswordTips').attr("class","errorTips").text("两次密码输入不一致，请重新输入");
    else
        $('#repeatedPasswordTips').attr("class","okTips").text("Available");
})

//4.邮箱合法性判断
var reE =  /^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$/
$('#email').on('blur',function () {
    if(this.value.trim() == '')
        $('#emailTips').text('');
    else if(!reE.test(this.value))
        $('#emailTips').attr("class","errorTips").text("邮箱格式错误");
    else
        $('#emailTips').attr("class","okTips").text("Available");
})

//5.电话合法性验证
var rePh = /^\d{11}$/
$('#phone').on('blur',function () {
    if(this.value.trim() == '')
        $('#phoneTips').text('');
    else if(!rePh.test(this.value))
        $('#phoneTips').attr("class","errorTips").text("电话号码格式错误");
    else
        $('#phoneTips').attr("class","okTips").text("Available");
})
