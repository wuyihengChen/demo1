function keepTwoDecimalFull(num) {
    num += '';//将数字转为字符串类型
    var pos_decimal = num.indexOf('.');
    if(pos_decimal < 0){
        pos_decimal = num.length;
        num += '.';
    }
    while (num.length <= pos_decimal + 2){
        num += '0';
    }
    return num;
}

function formatMoney(num){
    return '$'+keepTwoDecimalFull(num);
}

$(".cartItem").each(function (){
    var MainThis = $(this);
    $(this).on("blur","input",function (){
        var itemId = $(this).attr("name");
        $.ajax({
            data    :{quantity: this.value,
                itemId  : itemId},
            type    :"POST",
            url     :"AutoCart",
            success :function (data){
                if(data.total == 0){
                    $("#"+itemId).remove();
                }

                MainThis.children("#"+itemId).text(formatMoney(data.total));
                $("#subtotal").text(formatMoney(data.subtotal));

            }
        })
    })
})
