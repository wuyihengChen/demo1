$('#keyword').on("keyup",function () {
    var content = this.value;
    if(content.trim() == ''){
        $(".autoComplete").css("display","none");
        return;
    }
    $.ajax({
        type    :"GET",
        url     :"AutoComplete",
        data    :{keyword:content},
        success :function (data){
            var res = data.split(",");
            var divSon = "";
            var divSonId = "";
            $('.autoComplete').empty();
            for(var i=0;i<res.length;i++){
                divSonId = "divSon" + i;
                divSon += "<ul id="+divSonId+" class= divSon>"+res[i]+"</ul>";
            }
            if(res == "")
                return;
            $('.autoComplete').append(divSon);
            $('.autoComplete').css("display","block");

        }
    })
})

$('.autoComplete').on("click","ul",function (){
    $("#keyword").val(this.innerText);
    $(".autoComplete").css("display","none");
})