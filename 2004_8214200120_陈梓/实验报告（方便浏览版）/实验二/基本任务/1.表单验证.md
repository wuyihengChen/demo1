# 1.任务需求

  **账号管理模块**。在新建账号和修改账号信息页面上用AJAX技术进行表单验证，如“判断用户名是否已存在”等功能。

# 2.效果图展示

![image-20220105103245533](表单验证.assets/image-20220105103245533.png)

![image-20220105103412493](表单验证.assets/image-20220105103412493.png)

![image-20220105112443079](表单验证.assets/image-20220105112443079.png)



# 3.实现方法

## 3.1 策划表单功能

1. 用户是否填写必填项目（包括：ID、密码、确认密码等。否则submit提交无法通过）
2. 用户是否填写ID（用户名长度、符号限制）
3. 用户是否填写密码（密码格式限制）
4. 两次密码是否输入一致
5. 用户电话、邮箱格式是否正确。

## 3.2实现思路

1. 在jsp页面注明相应标签id并引入js脚本；
2. 利用jQuery绑定相应标签事件，涉及到用户名是否存在模块，在js代码中通过xml异步通信技术将用户名传到servlet中调用业务逻辑功能访问数据库判断用户名是否存在；（使用jQuery简化代码）其他验证未用到后端，直接使用正则表达式等判断。
3. 创建一个POJO来保存相应的结果，并利用fastjson转化为json对象传给xml;
4. 再到js中通过正则表达式对结果进行判断并处理结果。



# 4.核心代码

## 4.1 JSP部分

```jsp

    <tr>
        <td>User ID:</td>
        <td>
            <input type="text" name="username" id="username"/>
            <span id="usernameTips"></span>
        </td>
    </tr>

    <font color="red">${requestScope.msg}</font>

    <tr>
    </tr>
    <tr>
        <td>New password:</td>
        <td>
            <input type="password" name="password" id="password"/>
            <span id="passwordTips"></span>
        </td>

    </tr>
    <tr>
        <td>Repeat password:</td>
        <td>
            <input type="password" name="repeatedPassword" id="repeatedPassword"/>
            <span id="repeatedPasswordTips"></span>
        </td>

    </tr>
	<tr>
        <td>Email:</td>
        <td>
            <input type="text" size="40" value="${sessionScope.account.email}" name="account.email" id="email"/>
            <span id="emailTips"></span>
        </td> 
    </tr>

<script type="text/javascript" src="js/NewAccountForm.js"></script>、
```

## 4.2 servlet部分

主要是对**用户名是否存在**进行处理，其他部分未涉及

```java
@WebServlet(name = "UsernameExistServlet", value = "/UsernameExist")
public class UsernameExistServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        AccountService accountService = new AccountService();
        Account account = accountService.getAccount(username);

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        accountRes accountRes = new accountRes();//创建一个POJO 里面包括code和msg两个私有成员

        if(account == null){
            accountRes.setCode(0);
            accountRes.setMsg("Not Exist");
        }
        else {
            accountRes.setCode(1);
            accountRes.setMsg("Exist");
        }
        String str = JSON.toJSONString(accountRes);//将值对象转化为json格式
        out.print(str);
        out.flush();
        out.close();
    } 
```

## 4.3 js部分

```js
//1.用户合法性判断
var reN = /^\w{3,20}$/
$('#username').on('blur', function () {
    if (this.value.trim() == '') {
        $('#usernameTips').attr("class", "errorTips").text("用户名不为空");
        return;
    }
    if (reN.test(this.value)) {
        $.ajax({
            type: "GET",
            url: "UsernameExist?username=" + this.value,
            success: function (data) {
                if (data.code == 1)
                    $('#usernameTips').attr("class", "errorTips").text("用户名已存在，请重新输入");
                else if (data.code == 0)
                    $('#usernameTips').attr("class", "okTips").text("Available");
            }
        })
    }
    else
        $('#usernameTips').attr("class","errorTips").text("用户名是3-20位数字、字母和下划线！");
})

//2.密码合法性判断
var reP = /^[\w!-@#$%^&*]{6,20}$/
$('#password').on('blur',function () {
    if(this.value.trim() == '')
        $('#passwordTips').attr("class","errorTips").text("密码不为空");
    else if(!reP.test(this.value))
        $('#passwordTips').attr("class","errorTips").text("密码是6到20位字母、数字，还可包含@!#$%^&*-字符");
    else
        $('#passwordTips').attr("class","okTips").text("Available");
})

//3.判断两次输入密码一致性
$('#repeatedPassword').on('blur',function (){
    if(this.value.trim() == '')
        $('#repeatedPasswordTips').attr("class","errorTips").text("重复密码不为空");
    else if(!($('#password').value == this.value))
        $('#repeatedPasswordTips').attr("class","errorTips").text("两次密码输入不一致，请重新输入");
    else
        $('#repeatedPasswordTips').attr("class","okTips").text("Available");
})

//4.邮箱合法性判断
var reE =  /^[a-z0-9][\w\.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$/
$('#email').on('blur',function () {
    if(this.value.trim() == '')
        $('#emailTips').text('');
    else if(!reE.test(this.value))
        $('#emailTips').attr("class","errorTips").text("邮箱格式错误");
    else
        $('#emailTips').attr("class","okTips").text("Available");
})

//5.电话合法性验证
var rePh = /^\d{11}$/
$('#phone').on('blur',function () {
    if(this.value.trim() == '')
        $('#phoneTips').text('');
    else if(!rePh.test(this.value))
        $('#phoneTips').attr("class","errorTips").text("电话号码格式错误");
    else
        $('#phoneTips').attr("class","okTips").text("Available");
})
```

## 4.4 css部分

```css
.okTips{
  color:green;
}

.errorTips{
  color:red;
}
```