# 1.任务需求

 **日志功能：**数据库中添加日志信息表，给项目添加日志功能，用户登录后记录用户行为，比如浏览了哪些商品、将商品添加进购物车、生成订单等。

# 2.效果图展示

![image-20220112003050316](日志模块.assets/image-20220112003050316.png)

# 3.实现方法

在servlet中，每次页面跳转前，通过字符串拼接的方式获取当前请求的URL，并且拼接上用户行为（比如“用户登录）。再将得到的日志信息写入数据库。

# 4.核心代码

## 4.1 servlet

在每个servlet中页面跳转前加入类似语句

```java
if(account != null){
    HttpServletRequest httpRequest= request;
    String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
            + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

    LogService logService = new LogService();
    String logInfo = logService.logInfo(" ") + strBackUrl + " 用户登录";
    logService.insertLogInfo(account.getUsername(), logInfo);
}
```

## 4.2 Dao层查询

```java
public class LogDAOImpl implements LogDAO {

    private static final String insertLogString = "insert into log (logUserId, logInfo) VALUES (?, ?)";

    @Override
    public void insertLog(String username, String logInfo) {
        try {
            Connection connection = DBUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(insertLogString);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, logInfo);

            preparedStatement.executeUpdate();
            DBUtil.closePreparedStatement(preparedStatement);
            DBUtil.closeConnection(connection);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
```

