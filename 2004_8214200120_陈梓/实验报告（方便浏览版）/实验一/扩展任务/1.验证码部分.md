# 1.任务需求

**添加验证码功能：**在用户注册和用户登录模块中添加验证码功能。

# 2.效果图展示

![image-20220109104917056](验证码部分.assets/image-20220109104917056.png)

# 3.实现方法

1. 导入谷歌验证码jar包；
2. 在web.xml中配置Servlet程序；
3. 在表单中使用img标签显示验证码图片；
4. 在服务器获取谷歌生成的验证码和客户端发来的验证码比较。



# 4.核心代码

## 4.1 xml中配置

```xml
<servlet>
    <servlet-name>KaptchaServlet</servlet-name>
    <servlet-class>com.google.code.kaptcha.servlet.KaptchaServlet</servlet-class>
</servlet>
    <servlet-mapping>
        <servlet-name>KaptchaServlet</servlet-name>
        <url-pattern>/Kaptcha.jpg</url-pattern>
    </servlet-mapping>
```

## 4.2 img标签显示验证码

```jsp
<tr>
    <td>verification code:</td>
    <td>
        <input type="text" name="code" style="width: 80px">
        <a href = "http://localhost:8080/mypetstore/Kaptcha.jpg">
            <img src="http://localhost:8080/mypetstore/Kaptcha.jpg" style="vertical-align: middle; height: 24px ;" >
        </a>
    </td>
</tr>
```

## 4.3 谷歌servlet程序响应验证码图片

大概逻辑是：生成一系列随机验证码数字并加入干扰元素，清除浏览器缓存，将生成的图片数据流响应到服务端，并且将答案key传入session域中保存在KAPTCHA_SESSION_KEY中。

```java
public class KaptchaServlet extends HttpServlet implements Servlet {
    private Properties props = new Properties();
    private Producer kaptchaProducer = null;
    private String sessionKeyValue = null;
    private String sessionKeyDateValue = null;

    public KaptchaServlet() {
    }

    public void init(ServletConfig conf) throws ServletException {
        super.init(conf);
        ImageIO.setUseCache(false);
        Enumeration initParams = conf.getInitParameterNames();

        while(initParams.hasMoreElements()) {
            String key = (String)initParams.nextElement();
            String value = conf.getInitParameter(key);
            this.props.put(key, value);
        }

        Config config = new Config(this.props);
        this.kaptchaProducer = config.getProducerImpl();
        this.sessionKeyValue = config.getSessionKey();//将keyValue设置为KAPTCHA_SESSION_KEY
        this.sessionKeyDateValue = config.getSessionDate();
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setDateHeader("Expires", 0L);
        resp.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        resp.addHeader("Cache-Control", "post-check=0, pre-check=0");
        resp.setHeader("Pragma", "no-cache");
        resp.setContentType("image/jpeg");
        String capText = this.kaptchaProducer.createText();
        req.getSession().setAttribute(this.sessionKeyValue, capText);
        req.getSession().setAttribute(this.sessionKeyDateValue, new Date());
        BufferedImage bi = this.kaptchaProducer.createImage(capText);
        ServletOutputStream out = resp.getOutputStream();
        ImageIO.write(bi, "jpg", out);
    }
}
```

## 4.4 SignonServlet接收验证码答案并删除

```java
String token =(String)request.getSession().getAttribute(KAPTCHA_SESSION_KEY);//获取验证码
request.getSession().removeAttribute(KAPTCHA_SESSION_KEY);//删除验证码

if (token == null || !token.equalsIgnoreCase(code)){//忽略大小写比较验证码内容
            request.setAttribute("codeError","Invalid Verification code or Verification code is empty.");
            request.getRequestDispatcher(SIGNON).forward(request,response);
        }
```