<c:if test="${!empty sessionScope.myList}">
	<div>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#Catalog").hide().fadeIn();
			});
		</script>
	</div>
	<p>Pet Favorites <br />
	Shop for more of your favorite pets here.</p>
	<ul>
		<c:forEach var="product" items="${sessionScope.myList}">
			<li>
				<a href="viewProduct?productId=${product.productId}">${product.name}</a>(${product.productId})
			</li>
		</c:forEach>
	</ul>

</c:if>
