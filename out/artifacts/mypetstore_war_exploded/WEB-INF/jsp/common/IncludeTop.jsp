<%--
  Created by IntelliJ IDEA.
  User: wuyiheng
  Date: 2021/10/29
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">

<head>
    <link rel="StyleSheet" href="css/jpetstore.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/aboveTool.css" type="text/css" media="screen"/>
    <meta name="generator"
          content="HTML Tidy for Linux/x86 (vers 1st November 2002), see www.w3.org"/>
    <title>JPetStore Demo</title>
    <meta content="text/html; charset=windows-1252"
          http-equiv="Content-Type"/>
    <meta http-equiv="Cache-Control" content="max-age=0"/>
    <meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="Expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
    <style>
        #live2dcanvas {
            border: 0 !important;
        }
    </style>
    <script src="js/L2Dwidget.0.min.js"></script>
    <script src="js/L2Dwidget.min.js"></script>
</head>

<body>

<script>
    L2Dwidget.init({
        display: {
            superSample: 1,
            width: 220,
            height: 370,
            position: "right",
            hOffset: 0,
            vOffset: 0,
        },
    });
</script>
<div id="Header">

    <div id="Logo">
        <a href="main"><img src="images/logo-topbar.gif"/></a>
    </div>
</div>

<div id="Menu">
    <div id="MenuContent">
        <a href="viewCart"><img align="middle" name="img_cart" src="images/cart.gif"/></a>
        <img align="middle" src="images/separator.gif"/>
        <c:if test="${sessionScope.account == null}">
            <a href="signonForm">Sign In</a>
        </c:if>
        <c:if test="${sessionScope.account != null}">
            <c:if test="${!sessionScope.authenticated}">
                <a href="signonForm">Sign In</a>
            </c:if>
        </c:if>
        <c:if test="${sessionScope.account != null}">
            <c:if test="${sessionScope.authenticated}">
                <a href="signoff">Sign Out</a>
                <img align="middle" src="images/separator.gif"/>
                <a href="editAccountForm">My Account</a>
            </c:if>
        </c:if>
        <img align="middle" src="images/separator.gif"/> <a
            href="help.html">?</a></div>
</div>

<div id="Search">
    <div id="SearchContent" class="nav">
        <form action="search" method="post" id="groupOfSearch">
            <input type="text" name="keyword" size="14" id="keyword"/>
            <input type="submit" name="searchProducts" value="Search"/>

            <div class="autoComplete">
            </div>
        </form>
    </div>

</div>

<div id="QuickLinks">
    <a href="viewCategory?categoryId=FISH">
        <img src="images/sm_fish.gif"/>
    </a>
    <img src="images/separator.gif"/>
    <a href="viewCategory?categoryId=DOGS">
        <img
                src="images/sm_dogs.gif"/></a>
    <img src="images/separator.gif"/>
    <a href="viewCategory?categoryId=REPTILES">
        <img
                src="images/sm_reptiles.gif"/></a>
    <img
            src="images/separator.gif"/>
    <a href="viewCategory?categoryId=CATS"><img
            src="images/sm_cats.gif"/></a>
    <img src="images/separator.gif"/>
    <a href="viewCategory?categoryId=BIRDS">
        <img
                src="images/sm_birds.gif"/>
    </a>
</div>

</div>

<script type="text/javascript" src="js/autoComplete.js"></script>
<div id="Content">
${requestScope.message}