<%@ include file="../common/IncludeTop.jsp"%>
<div id="Welcome">
    <div id="WelcomeContent" class="welcome">
        Welcome to MyPetStore!
    </div>
</div>

<div id="Main">
    <div id="Sidebar">
        <div id="SidebarContent">

            <div id="flip1" style="height: 40px; width: 100%; alignment: left; border-radius: 5px;">
                <a href="viewCategory?categoryId=FISH"><img src="images/fish_icon.gif" /></a>
                <i class="arrowMored"></i>
            </div>
            <div id="panel1" style="height: 70px; width: 100%; alignment: left; border-radius: 5px;">
                <%--            <br/> Saltwater, Freshwater <br/>--%>
                <img src="images/fish1.gif" style="width: 50px; height: 50px;"/>
                <img src="images/fish2.gif" style="width: 50px; height: 50px;"/>
                <img src="images/fish3.gif" style="width: 50px; height: 50px;"/>
            </div>


            <div id="flip2" style="height: 40px; width: 100%; alignment: left; border-radius: 5px;">
                <a href="viewCategory?categoryId=DOGS"><img src="images/dogs_icon.gif" /></a>
                <i class="arrowMored"></i>
            </div>
            <div id="panel2" style="height: 70px; width: 100%; alignment: left; border-radius: 5px;">
                <%--            <br/> Saltwater, Freshwater <br/>--%>
                <img src="images/dog3.gif" style="width: 50px; height: 50px;"/>
                <img src="images/dog4.gif" style="width: 50px; height: 50px;"/>
                <img src="images/dog5.gif" style="width: 50px; height: 50px;"/>
            </div>


            <div id="flip3" style="height: 40px; width: 100%; alignment: left; border-radius: 5px;">
                <a href="viewCategory?categoryId=CATS"><img src="images/cats_icon.gif" /></a>
                <i class="arrowMored"></i>
            </div>
            <div id="panel3" style="height: 70px; width: 100%; alignment: left; border-radius: 5px;">
                <%--            <br/> Saltwater, Freshwater <br/>--%>
                <img src="images/cat1.gif" style="width: 50px; height: 50px;"/>
                <img src="images/cat2.gif" style="width: 50px; height: 50px;"/>
            </div>


            <div id="flip4" style="height: 40px; width: 100%; alignment: left; border-radius: 5px;">
                <a href="viewCategory?categoryId=REPTILES"><img src="images/reptiles_icon.gif" /></a>
                <i class="arrowMored"></i>
            </div>
            <div id="panel4" style="height: 70px; width: 100%; alignment: left; border-radius: 5px;">
                <%--            <br/> Saltwater, Freshwater <br/>--%>
                <img src="images/snake1.gif" style="width: 50px; height: 50px;"/>
                <img src="images/lizard1.gif" style="width: 50px; height: 50px;"/>
            </div>


            <div id="flip5" style="height: 40px; width: 100%; alignment: left; border-radius: 5px;">
                <a href="viewCategory?categoryId=BIRDS"><img src="images/birds_icon.gif" /></a>
                <i class="arrowMored"></i>
            </div>
            <div id="panel5" style="height: 70px; width: 100%; alignment: left; border-radius: 5px;">
                <%--            <br/> Saltwater, Freshwater <br/>--%>
                <img src="images/bird1.gif" style="width: 50px; height: 50px;"/>
                <img src="images/bird2.gif" style="width: 50px; height: 50px;"/>
            </div>
        </div>
    </div>

    <div id="MainImage">
        <div id="MainImageContent">
            <map name="estoremap">
                <area onmouseout="javascript:hide(this, 'BirdsTip');" onMouseOver="javascript:show(this,'BirdsTip');" alt="Birds" coords="72,2,280,250" href="viewCategory?categoryId=BIRDS" shape="rect" />
                <area onmouseout="javascript:hide(this, 'FishTip');" onMouseOver="javascript:show(this,'FishTip');" alt="Fish" coords="2,180,72,250" href="viewCategory?categoryId=FISH" shape="rect" />
                <area onmouseout="javascript:hide(this, 'DogsTip');" onMouseOver="javascript:show(this,'DogsTip');" alt="Dogs" coords="60,250,130,320" href="viewCategory?categoryId=DOGS" shape="rect" />
                <area onmouseout="javascript:hide(this, 'ReptilesTip');" onMouseOver="javascript:show(this,'ReptilesTip');" alt="Reptiles" coords="140,270,210,340" href="viewCategory?categoryId=REPTILES" shape="rect" />
                <area onmouseout="javascript:hide(this, 'CatsTip');" onMouseOver="javascript:show(this,'CatsTip');" alt="Cats" coords="225,240,295,310" href="viewCategory?categoryId=CATS" shape="rect" />
                <area onmouseout="javascript:hide(this, 'BirdsTip');" onMouseOver="javascript:show(this,'BirdsTip');" alt="Birds" coords="280,180,350,250" href="viewCategory?categoryId=BIRDS" shape="rect" />
            </map>
            <img height="355" src="images/splash.gif" align="middle" usemap="#estoremap" width="350" />
            <div id="inform" style="display: none"></div>
        </div>
    </div>

    <div class="box" id="box">
        <div class="inner">
            <!--轮播图-->
            <ul class="ul1">
                <li><a onmouseout="javascript:hide(this, 'DogsTip');" onmouseover="javascript:show(this,'DogsTip');" href="viewCategory?categoryId=DOGS"><img src="images/slide_cheems.png" alt=""></a></li>
                <li><a onmouseout="javascript:hide(this, 'CatsTip');" onmouseover="javascript:show(this,'CatsTip');" href="viewCategory?categoryId=CATS"><img src="images/slide_cat.png" alt=""></a></li>
                <li><a onmouseout="javascript:hide(this, 'BirdsTip');" onmouseover="javascript:show(this,'BirdsTip');" href="viewCategory?categoryId=BIRDS"><img src="images/slide_bird.png" alt=""></a></li>
            </ul>
            <ol class="bar">
            </ol>
            <!--左右焦点-->
            <div id="arr">
                <span id="left"><</span>
                <span id="right">></span>
            </div>
        </div>
    </div>
    <div id="Separator">&nbsp;</div>

    <div id="BirdsTip" style="position:absolute;display:none;border:1px solid silver;background:silver;">
        Category:   BIRDS
        <br>
        ----------------------------
        <br>
        ProductID and Name
        <br>
        AV-CB-01   Amazon Parrot
        <br>
        AV-SB-02   Finch
    </div>
    <div id="FishTip" style="position:absolute;display:none;border:1px solid silver;background:silver;">
        Category:   FISH
        <br>
        ----------------------------
        <br>
        ProductID and Name

        <br>
        FI-FW-01   Koi
        <br>
        FI-FW-02   Goldfish
        <br>
        FI-SW-01   Angelfish
        <br>
        FI-SW-02   Tiger Shark
    </div>
    <div id="DogsTip" style="position:absolute;display:none;border:1px solid silver;background:silver;">
        Category:   DOGS
        <br>
        ----------------------------
        <br>
        ProductID and Name
        <br>
        K9-BD-01   Bulldog
        <br>
        K9-CW-01   Chihuahua
        <br>
        K9-DL-01   Dalmation
        <br>
        K9-PO-02   Poodle
        <br>
        K9-RT-01   Golden Retriever
        <br>
        K9-RT-02   Labrador Retriever
    </div>
    <div id="ReptilesTip" style="position:absolute;display:none;border:1px solid silver;background:silver;">
        Category:   REPTILES
        <br>
        ----------------------------
        <br>
        ProductID and Name
        <br>
        RP-LI-02   Iguana
        <br>
        RP-SN-01   Rattlesnake
    </div>
    <div id="CatsTip" style="position:absolute;display:none;border:1px solid silver;background:silver;">
        Category:   CATS
        <br>
        ----------------------------
        <br>
        ProductID and Name
        <br>
        FL-DLH-02  Persian
        <br>
        FL-DSH-01  Manx
    </div>
</div>
<%@include file="../common/IncludeBottom.jsp"%>
<link rel="stylesheet" href="css/main.css" type="text/css" media="screen">
<script type="text/javascript" src="js/Main.js"></script>