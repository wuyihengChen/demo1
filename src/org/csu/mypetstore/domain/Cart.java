package org.csu.mypetstore.domain;

//import com.ibatis.common.util.PaginatedArrayList;
import org.csu.mypetstore.persistence.CartDAO;
import org.csu.mypetstore.persistence.Impl.CartDAOImpl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Cart implements Serializable {

  private CartDAO cartDAO;
  public Cart(){
    cartDAO = new CartDAOImpl();
  }
  private static final long serialVersionUID = 8329559983943337176L;
  private final Map<String, CartItem> itemMap = Collections.synchronizedMap(new HashMap<String, CartItem>());
  private  List<CartItem> itemList = new ArrayList<CartItem>();

  public Iterator<CartItem> getCartItems() {
    return itemList.iterator();
  }

  public List<CartItem> getCartItemList() {
    return itemList;
  }

  public int getNumberOfItems() {
    return itemList.size();
  }

  public Iterator<CartItem> getAllCartItems() {
    return itemList.iterator();
  }

  public boolean containsItemId(String itemId) {
    return itemMap.containsKey(itemId);
  }

  public void addItem(Item item, boolean isInStock, String userid) {
    CartItem cartItem = (CartItem) itemMap.get(item.getItemId());
    if (cartItem == null) {
      cartItem = new CartItem();
      cartItem.setItem(item);
      cartItem.setQuantity(0);
      cartItem.setInStock(isInStock);
      itemMap.put(item.getItemId(), cartItem);
      itemList.add(cartItem);
      if(userid != null && !userid.equals("")){
        //数据库中插入
        cartDAO.insertCartItem(cartItem, userid);
      }
    }
    cartItem.incrementQuantity();
    cartDAO.incrementQuantity(cartItem);
  }

  public Item removeItemById(String itemId) {
    CartItem cartItem = (CartItem) itemMap.remove(itemId);
    if (cartItem == null) {
      return null;
    } else {
      itemList.remove(cartItem);
      cartDAO.removeItemById(cartItem);
      return cartItem.getItem();
    }
  }

  public void incrementQuantityByItemId(String itemId) {
    CartItem cartItem = (CartItem) itemMap.get(itemId);
    cartItem.incrementQuantity();
    cartDAO.incrementQuantity(cartItem);
  }

  public void setQuantityByItemId(String itemId, int quantity) {
    CartItem cartItem = (CartItem) itemMap.get(itemId);
    cartItem.setQuantity(quantity);
//    cartDAO.incrementQuantity(cartItem);
    cartDAO.updateQuantityByItemId(cartItem, quantity);
  }

  public BigDecimal getSubTotal() {
    BigDecimal subTotal = new BigDecimal("0");
    Iterator<CartItem> items = getAllCartItems();
    while (items.hasNext()) {
      CartItem cartItem = (CartItem) items.next();
      Item item = cartItem.getItem();
      BigDecimal listPrice = item.getListPrice();
      BigDecimal quantity = new BigDecimal(String.valueOf(cartItem.getQuantity()));
      subTotal = subTotal.add(listPrice.multiply(quantity));
    }
    return subTotal;
  }

  public void clear(Cart cart,String workingItemId) {
    workingItemId = null;
    itemList.clear();
    itemMap.clear();
  }

  public List<CartItem> getCartItemListByUserid(String userid){
    return cartDAO.getCartItemListByUserid(userid);
  }

  public void addItem1(CartItem cartItem){
    itemMap.put(cartItem.getItem().getItemId(), cartItem);
    itemList.add(cartItem);
  }

  public void removeAllCartItemsByUserid(String userid){
    cartDAO.removeAllCartItemsByUserid(userid);
  }

  public void addItemQuantity(Item item, boolean isInStock,int quantity) {
    CartItem cartItem = (CartItem) itemMap.get(item.getItemId());
    if (cartItem == null) {
      cartItem = new CartItem();
      cartItem.setItem(item);
      cartItem.setQuantity(quantity);
      cartItem.setInStock(isInStock);
      itemMap.put(item.getItemId(), cartItem);
      itemList.add(cartItem);
    }
    else {
      //cartItem.incrementQuantity();
      cartItem.setQuantity(quantity);
    }
  }
}
