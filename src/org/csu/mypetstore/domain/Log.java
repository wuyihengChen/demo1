package org.csu.mypetstore.domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {

    protected static Date data=null;
    protected static StringBuilder bb=new StringBuilder();
    protected static String marsk=" [ERROE] ";

    protected static String time()
    {
        data = new Date();
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(data);
    }


    //日志函数，拼接字符串
    synchronized public static String log(Object ...s)
    {
        bb.delete(0, bb.length());
        bb.append(time());//日志内容，先时间
        bb.append(marsk);

        bb.append("  ");

        for(Object str : s)
        {
            bb.append(str);
            bb.append(" ");
        }
        return bb.toString();
    }

    public static String logInfomation(Object ...s)
    {
        marsk = " ";
        return log(s);
    }


}
