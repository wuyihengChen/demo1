package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Cart;

import org.csu.mypetstore.domain.Order;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.service.CatalogService;
import org.csu.mypetstore.service.LogService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "NewOrderFormServlet", value = "/newOrderForm")
public class NewOrderFormServlet extends HttpServlet {

    private static final String ERROR = "/WEB-INF/jsp/common/Error.jsp";
    private static final String SIGNON = "/WEB-INF/jsp/account/SignonForm.jsp";
    private static final String NEW_ORDER = "/WEB-INF/jsp/order/NewOrder.jsp";

    private Order order = new Order();
    private boolean shippingAddressRequired;
    private boolean confirmed;
    private List<Order> orderList;
    private String msg;
    private String workingItemId;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        Cart cart = (Cart) session.getAttribute("cart");
        workingItemId = (String) session.getAttribute("workingItemId");
        order = new Order();
        shippingAddressRequired = false;
        confirmed = false;
        orderList = null;
        session.setAttribute("order",order);

        if(account == null || account.getUsername() == null || !request.authenticate(response)){
            msg = "You must sign on before attempting to check out.  Please sign on and try checking out again.";
            request.setAttribute("msg",msg);
            request.getRequestDispatcher(SIGNON).forward(request,response);
        }
        //初始化订单
        else if(cart != null) {
            order.initOrder(account,cart);
       session.setAttribute("order",order);

            //保存日志
            if (account != null) {
                HttpServletRequest httpRequest = request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 跳转到新订单页面";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }


            request.getRequestDispatcher(NEW_ORDER).forward(request,response);
        }
        else {
            msg = "An order could not be created because a cart could not be found.";
            request.setAttribute("msg",msg);

            if (account != null) {
                HttpServletRequest httpRequest = request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 生成订单页面信息错误";
                logService.insertLogInfo(account.getUsername(), logInfo);

                request.getRequestDispatcher(ERROR).forward(request,response);
             }
        }
    }
}
