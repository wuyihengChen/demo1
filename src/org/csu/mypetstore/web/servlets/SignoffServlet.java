package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.service.LogService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "SignoffServlet", value = "/signoff")
public class SignoffServlet extends HttpServlet {

    private static final String MAIN = "/WEB-INF/jsp/catalog/Main.jsp";

    private Account account;
    private boolean authenticated = false;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //先获取session中的account，因为之后要new account
        HttpSession session=request.getSession();
        account= (Account) session.getAttribute("account");
        if(account!=null){
            String StrBackUrl="Http://"+ request.getServerName()+":"+request.getServerPort()
                    +request.getContextPath()+request.getServletPath()+"?"
                    +(request.getQueryString());
            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + StrBackUrl + " 用户退出登录 ";
            logService.insertLogInfo(account.getUsername(),logInfo);
        }

        request.getSession().invalidate();
        account = new Account();
        List<Product> myList = null;
        authenticated = false;

        response.sendRedirect(request.getContextPath()+"/main");
    }
}
