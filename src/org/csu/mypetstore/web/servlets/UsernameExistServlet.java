package org.csu.mypetstore.web.servlets;

import com.alibaba.fastjson.JSON;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.util.accountRes;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

@WebServlet(name = "UsernameExistServlet", value = "/UsernameExist")
public class UsernameExistServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        AccountService accountService = new AccountService();
        Account account = accountService.getAccount(username);

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();

        accountRes accountRes = new accountRes();

        if(account == null){
            accountRes.setCode(0);
            accountRes.setMsg("Not Exist");
        }
        else {
            accountRes.setCode(1);
            accountRes.setMsg("Exist");
        }
        String str = JSON.toJSONString(accountRes);
        out.print(str);
        out.flush();
        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
