package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.service.CatalogService;
import org.csu.mypetstore.service.LogService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@WebServlet(name = "EditAccountServlet", value = "/editAccount")
public class EditAccountServlet extends HttpServlet {

    private static final String MAIN = "/WEB-INF/jsp/catalog/Main.jsp";
    private static final String EDIT_ACCOUNT = "/WEB-INF/jsp/account/EditAccountForm.jsp";

    private Account account = new Account();
    private String repeatedPassword;
    private String msg;
    private String username;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AccountService accountService = new AccountService();
        HttpSession session = request.getSession();
        account = (Account) session.getAttribute("account");
        account = accountService.getAccount(account.getUsername());
        username = account.getUsername();

        this.account = new Account();
        account.setUsername(username);
        account.setPassword(request.getParameter("password"));
        repeatedPassword = request.getParameter("repeatedPassword");
        account.setFirstName(request.getParameter("account.firstName"));
        account.setLastName(request.getParameter("account.lastName"));
        account.setEmail(request.getParameter("account.email"));
        account.setPhone(request.getParameter("account.phone"));
        account.setAddress1(request.getParameter("account.address1"));
        account.setAddress2(request.getParameter("account.address2"));
        account.setCity(request.getParameter("account.city"));
        account.setState(request.getParameter("account.state"));
        account.setZip(request.getParameter("account.zip"));
        account.setCountry(request.getParameter("account.country"));
        account.setLanguagePreference(request.getParameter("account.languagePreference"));
        account.setFavouriteCategoryId(request.getParameter("account.favouriteCategoryId"));
        account.setListOption(request.getParameter("account.listOption")!=null);
        account.setBannerOption(request.getParameter("account.bannerOption")!=null);

        if(!account.getPassword().equals(repeatedPassword) && account.getPassword() != null){
            msg = "Two passwords do not match.";
            request.setAttribute("msg",msg);
            request.getRequestDispatcher(EDIT_ACCOUNT).forward(request,response);
        }
        else {
            accountService = new AccountService();
            accountService.updateAccount(account);
            account = accountService.getAccount(account.getUsername());
            CatalogService catalogService = new CatalogService();
            List<Product> myList = catalogService.getProductListByCategory(account.getFavouriteCategoryId());
            session.setAttribute("account",account);
            session.setAttribute("myList",myList);

            if(account != null){
                HttpServletRequest httpRequest= request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 已修改账户信息 ";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }

            response.sendRedirect(request.getContextPath()+"/main");
        }
    }
}
