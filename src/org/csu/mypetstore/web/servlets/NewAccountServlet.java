package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.service.AccountService;
import org.csu.mypetstore.service.CatalogService;
import org.csu.mypetstore.service.LogService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "NewAccountServlet", value = "/newAccount")
public class NewAccountServlet extends HttpServlet {

    private static final String MAIN = "/WEB-INF/jsp/catalog/Main.jsp";
    private static final String NEW_ACCOUNT = "/WEB-INF/jsp/account/NewAccountForm.jsp";

    private AccountService accountService = new AccountService();


    private List<Product> myList;
    private boolean authenticated;
    private Account account;
    private String repeatedPassword;
    private String msg;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        CatalogService catalogService = new CatalogService();
        getParameters(request);//获取参数
        if(!validation()){
            request.setAttribute("msg",msg);
            request.getRequestDispatcher(NEW_ACCOUNT).forward(request,response);
        }
        else{
            accountService.insertAccount(account);
            myList = catalogService.getProductListByCategory(account.getFavouriteCategoryId());
            authenticated = true;
            HttpSession session = request.getSession();
            session.setAttribute("account", account);
            session.setAttribute("myList", myList);
            session.setAttribute("authenticated", authenticated);
            //日志part
            if(account != null){

                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + request.getContextPath() + request.getServletPath() + "?" + (request.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + "完成了新账号注册";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }
//            request.getRequestDispatcher(MAIN).forward(request,response);
            response.sendRedirect(request.getContextPath()+"/main");
        }

    }
    //验证
    private boolean validation(){
        if(account.getUsername() == null || account.getUsername().trim().equals("")){
            msg = "Your name is required.";
            return false;
        }
        if(accountService.getAccount(account.getUsername()) != null){
            msg = "Your name is occupied.";
            return false;
        }
        if(account.getPassword() == null || account.getPassword().trim().equals("")){
            msg = "Your password is required.";
            return false;
        }
        if (!account.getPassword().equals(repeatedPassword)){
            msg = "Two passwords do not match.";
            return false;
        }
        return true;
    }

    private void getParameters(HttpServletRequest request){
        this.account = new Account();
        account.setUsername(request.getParameter("username"));
        account.setPassword(request.getParameter("password"));
        repeatedPassword = request.getParameter("repeatedPassword");
        account.setFirstName(request.getParameter("account.firstName"));
        account.setLastName(request.getParameter("account.lastName"));
        account.setEmail(request.getParameter("account.email"));
        account.setPhone(request.getParameter("account.phone"));
        account.setAddress1(request.getParameter("account.address1"));
        account.setAddress2(request.getParameter("account.address2"));
        account.setCity(request.getParameter("account.city"));
        account.setState(request.getParameter("account.state"));
        account.setZip(request.getParameter("account.zip"));
        account.setCountry(request.getParameter("account.country"));
        account.setLanguagePreference(request.getParameter("account.languagePreference"));
        account.setFavouriteCategoryId(request.getParameter("account.favouriteCategoryId"));
        account.setListOption(request.getParameter("account.listOption")!=null);
        account.setBannerOption(request.getParameter("account.bannerOption")!=null);

    }
}
