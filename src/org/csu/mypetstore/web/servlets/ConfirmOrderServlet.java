package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Order;
import org.csu.mypetstore.service.LogService;
import org.csu.mypetstore.service.OrderService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Date;

@WebServlet(name = "ConfirmOrderServlet", value = "/confirmOrder")
public class ConfirmOrderServlet extends HttpServlet {

    private static final String CONFIRM_ORDER = "/WEB-INF/jsp/order/ConfirmOrder.jsp";

    private Order order;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderService orderService = new OrderService();
        HttpSession session = request.getSession();
        order = (Order) session.getAttribute("order");
        getParameter(request);//获取表单提交参数，添加到order中

        Account account = (Account)session.getAttribute("account");
        if(account != null){
            HttpServletRequest httpRequest= request;
            String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                    + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

            LogService logService = new LogService();
            String logInfo = logService.logInfo(" ") + strBackUrl + " 确认生成订单 ";
            logService.insertLogInfo(account.getUsername(), logInfo);
        }


        session.setAttribute("order",order);
        request.setAttribute("order",order);
        request.getRequestDispatcher(CONFIRM_ORDER).forward(request,response);
    }

    public void getParameter(HttpServletRequest request){
        if(request.getParameter("order.BillToFirstName") != null) {//区别于未修改ship的表单
            order.setBillToFirstName(request.getParameter("order.BillToFirstName"));
            order.setBillToLastName(request.getParameter("order.BillToLastName"));
            order.setBillAddress1(request.getParameter("order.BillAddress1"));
            order.setBillAddress2(request.getParameter("order.BillAddress2"));
            order.setBillCity(request.getParameter("order.BillCity"));
            order.setBillState(request.getParameter("order.BillState"));
            order.setBillZip(request.getParameter("order.BillZip"));
            order.setBillCountry(request.getParameter("order.BillCountry"));
        }
        else if(request.getParameter("order.shipToFirstName") != null) {
            order.setShipToFirstName(request.getParameter("order.shipToFirstName"));
            order.setShipToLastName(request.getParameter("order.shipToLastName"));
            order.setShipAddress1(request.getParameter("order.shipAddress1"));
            order.setShipAddress2(request.getParameter("order.shipAddress2"));
            order.setShipCity(request.getParameter("order.shipCity"));
            order.setShipState(request.getParameter("order.shipState"));
            order.setShipZip(request.getParameter("order.shipZip"));
            order.setShipCountry(request.getParameter("order.shipCountry"));
        }
    }
}
