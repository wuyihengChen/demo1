package org.csu.mypetstore.web.servlets;

import com.alibaba.fastjson.JSON;
import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Cart;
import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.service.LogService;
import org.csu.mypetstore.util.cartRes;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Iterator;

@WebServlet(name = "AutoCartServlet", value = "/AutoCart")
public class AutoCartServlet extends HttpServlet {

    private Cart cart = new Cart();
    private int quantity;
    private String itemId;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        doPost(request,response);
//        Iterator<CartItem> cartItems = cart.getAllCartItems();
//        while(cartItems.hasNext()){
//            CartItem cartItem = (CartItem) cartItems.next();
//            itemId = cartItem.getItem().getItemId();
//
//            //日志模块
//            Account account = (Account) session.getAttribute("account");
//            if(account != null){
//                HttpServletRequest httpRequest = request;
//                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
//                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());
//                LogService logService = new LogService();
//                String logInfo = logService.logInfo(" ") + strBackUrl + "更新购物车商品数量";
//                logService.insertLogInfo(account.getUsername(),logInfo);
//            }
//
//            //修改数据库内信息
//            try{
//                cart.setQuantityByItemId(itemId,quantity);//更新对应购物车内商品数量
//                CartRes.setTotal(cartItem.getTotal());
//                if(quantity < 1)
//                    cartItems.remove();
//                break;
//            }catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        CartRes.setSubtotal(cart.getSubTotal());
//
//        //发送数据到服务端
//
//        response.setContentType("application/json");
//        PrintWriter out = response.getWriter();
//        String str = JSON.toJSONString(CartRes);
//        out.print(str);
//        out.flush();
//        out.close();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        cart = (Cart) session.getAttribute("cart");
        quantity = Integer.parseInt(request.getParameter("quantity"));
        itemId = request.getParameter("itemId");

        cart.setQuantityByItemId(itemId,quantity);//更新对应购物车内商品数量

        cartRes CartRes = new cartRes();//存放数据
        Iterator<CartItem> cartItems = cart.getAllCartItems();

        CartItem cartItem = new CartItem();
        while (cartItems.hasNext()){
            cartItem = (CartItem) cartItems.next();

            if(cartItem.getItem().getItemId().equals(itemId) )
                break;
        }
        if(quantity < 1) {
            cartItems.remove();
            cart.removeItemById(cartItem.getItem().getItemId());
        }
        CartRes.setTotal(cartItem.getTotal());
        CartRes.setSubtotal(cart.getSubTotal());

        //发送数据到服务端

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String str = JSON.toJSONString(CartRes);
        out.print(str);
        out.flush();
        out.close();
    }
}
