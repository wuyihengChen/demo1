package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Order;
import org.csu.mypetstore.service.LogService;
import org.csu.mypetstore.service.OrderService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ViewOrderServlet", value = "/viewOrder")
public class ViewOrderServlet extends HttpServlet {

    private static final String VIEW_ORDER = "/WEB-INF/jsp/order/ViewOrder.jsp";
    private static final String ERROR = "/WEB-INF/jsp/common/Error.jsp";
    private int orderId;
    private String msg;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");

        OrderService orderService = new OrderService();

        Order order = (Order) session.getAttribute("order");
        if(request.getParameter("orderId")!=null){
            orderId = Integer.parseInt(request.getParameter("orderId"));
        }else {
            orderId = order.getOrderId();
        }
        order = orderService.getOrder(orderId);

        session.setAttribute("order",order);

        if(account.getUsername() != null && account.getUsername().equals(order.getUsername())){

            if (account != null) {
                // HttpServletRequest httpRequest= request;
                String strBackUrl = "http://" + request.getServerName() + ":" + request.getServerPort()
                        + request.getContextPath() + request.getServletPath() + "?" + (request.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 查看订单 " + order.getOrderId();
                logService.insertLogInfo(account.getUsername(), logInfo);
            }
                request.getRequestDispatcher(VIEW_ORDER).forward(request,response);
        }
        else {
            order = null;
            msg = "You may only view your own orders.";
            request.setAttribute("msg",msg);
            request.getRequestDispatcher(ERROR).forward(request,response);
        }
    }
}
