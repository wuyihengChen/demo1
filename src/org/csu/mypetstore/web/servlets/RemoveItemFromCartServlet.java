package org.csu.mypetstore.web.servlets;

import org.csu.mypetstore.domain.Account;
import org.csu.mypetstore.domain.Cart;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.service.LogService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RemoveItemFromCartServlet extends HttpServlet {

    private static String VIEW_CART = "/WEB-INF/jsp/cart/Cart.jsp";
    private static String ERROR = "/WEB-INF/jsp/common/Error.jsp";

    private String workingItemId;
    private Cart cart;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        workingItemId = req.getParameter("workingItemId");

        HttpSession session = req.getSession();
        session.setAttribute("workingItemId",workingItemId);
        cart = (Cart) session.getAttribute("cart");

        Item item = cart.removeItemById(workingItemId);

        Account account = (Account)session.getAttribute("account");
        if(item == null){
            session.setAttribute("msg","Attempted to remove null CartItem from Cart");


            if(account != null){
                HttpServletRequest httpRequest= req;
                String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                        + httpRequest.getContextPath() + httpRequest.getServletPath() + "?" + (httpRequest.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " 物品为空，不能移除";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }
            req.getRequestDispatcher(ERROR).forward(req,resp);
        } else {

            if(account != null){
                String strBackUrl = "http://" + req.getServerName() + ":" + req.getServerPort()
                        + req.getContextPath() + req.getServletPath() + "?" + (req.getQueryString());

                LogService logService = new LogService();
                String logInfo = logService.logInfo(" ") + strBackUrl + " " + item + " 已从购物车中移除";
                logService.insertLogInfo(account.getUsername(), logInfo);
            }

            req.getRequestDispatcher(VIEW_CART).forward(req,resp);
        }
    }
}
