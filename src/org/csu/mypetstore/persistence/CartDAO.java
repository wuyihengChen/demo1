package org.csu.mypetstore.persistence;

import org.csu.mypetstore.domain.CartItem;

import java.util.List;

public interface CartDAO {
    void insertCartItem(CartItem cartItem, String userid);

    List<CartItem> getCartItemListByUserid(String userid);

    void incrementQuantity(CartItem cartItem);

    void removeItemById(CartItem cartItem);

    void updateQuantityByItemId(CartItem cartItem, int quantity);

    void removeAllCartItemsByUserid(String userid);
}
