package org.csu.mypetstore.persistence;

import org.csu.mypetstore.domain.Category;

import java.util.List;

public interface CategoryDAO {

    //获取所有大类
    List<Category> getCategoryList();

    //通过ID查找大类
    Category getCategory(String categoryId);
}
