package org.csu.mypetstore.service;

import org.csu.mypetstore.domain.CartItem;
import org.csu.mypetstore.domain.Category;
import org.csu.mypetstore.domain.Product;
import org.csu.mypetstore.domain.Item;
import org.csu.mypetstore.persistence.CartDAO;
import org.csu.mypetstore.persistence.CategoryDAO;
import org.csu.mypetstore.persistence.Impl.CartDAOImpl;
import org.csu.mypetstore.persistence.Impl.CategoryDAOImpl;
import org.csu.mypetstore.persistence.Impl.ProductDAOImpl;
import org.csu.mypetstore.persistence.ItemDAO;
import org.csu.mypetstore.persistence.ProductDAO;
import org.csu.mypetstore.persistence.Impl.ItemDAOImpl;

import java.util.List;

public class CatalogService {

    private CategoryDAO categoryDAO;
    private ProductDAO productDAO;
    private ItemDAO itemDAO;
    private CartDAO cartDAO;

    public CatalogService(){
        categoryDAO = new CategoryDAOImpl();
        productDAO = new ProductDAOImpl();
        itemDAO = new ItemDAOImpl();
        cartDAO = new CartDAOImpl();
    }

    public List<Category> getCategoryList() {
        return categoryDAO.getCategoryList();
    }

    public Category getCategory(String categoryId) {
        return categoryDAO.getCategory(categoryId);
    }

    public Product getProduct(String productId) {
        return productDAO.getProduct(productId);
    }

    public String getProductByCategoryNameAjax(String categoryId){
        List<Product> productList = productDAO.searchProductList("%" + categoryId.toLowerCase() + "%");
        String res="";
        for(int i = 0; i < productList.size(); i++){
            if(i > 0)
                res += "," + productList.get(i);
            else
                res += productList.get(i);
        }
        return res;
    }

    public List<Product> getProductListByCategory(String categoryId) {
        return productDAO.getProductListByCategory(categoryId);
    }

    public List<Product> searchProductList(String keyword) {
        return productDAO.searchProductList("%" + keyword.toLowerCase() + "%");
    }

    public List<Item> getItemListByProduct(String productId) {
        return itemDAO.getItemListByProduct(productId);
    }

    public Item getItem(String itemId) {
        return itemDAO.getItem(itemId);
    }

    public boolean isItemInStock(String itemId) {
        return itemDAO.getInventoryQuantity(itemId) > 0;
    }

    public void insertCartItem(CartItem cartItem, String userid){
        cartDAO.insertCartItem(cartItem, userid);
    }
}
