package org.csu.mypetstore.util;

import java.math.BigDecimal;

public class cartRes {

    private BigDecimal total;//单个种类总价
    private BigDecimal subtotal;//总价

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

}
